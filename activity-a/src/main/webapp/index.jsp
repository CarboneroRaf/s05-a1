<%@ page language="java" 
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="java.time.ZonedDateTime"
    import="java.time.ZoneId"
    import="java.time.format.DateTimeFormatter"
    import="java.time.format.FormatStyle"
    %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Carbonero Activity</title>
</head>
<body>
	<!-- Using the scriptlet tag, create a variable dateTime-->
	<!-- Use the LocalDateTime and DateTimeFormatter classes -->
	<!-- Change the pattern to "yyyy-MM-dd HH:mm:ss" -->
  	
  	<!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->

	<h1>Our Date and Time now is...</h1>
	
	<% System.out.println("Hello from JSP"); %>

	<% ZonedDateTime dateTime = ZonedDateTime.now();%>

	<%! ZonedDateTime dateTime = ZonedDateTime.now(); %>

	<% ZonedDateTime man = dateTime.withZoneSameInstant(ZoneId.of("Asia/Manila")); %>
	<% ZonedDateTime jap = dateTime.withZoneSameInstant(ZoneId.of("Asia/Tokyo")); %>
	<% ZonedDateTime germ = dateTime.withZoneSameInstant(ZoneId.of("Europe/Berlin")); %>
	
 	<% DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); %>
    <% String manTime = man.format(formatter); %>
    <% String japTime = jap.format(formatter); %>
    <% String germTime = germ.format(formatter); %>
	

	<ul>
		<li> Manila: <%= manTime %> </li>
		<li> Japan: <%= japTime %> </li>
		<li> Germany: <%= germTime %> </li>
	</ul>
	

	<%!
		private int initVar = 1;
		private int serviceVar = 1;
		private int destroyVar = 3;
	%>
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init"+initVar);
		}
		
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy"+destroyVar);
		}
	%>
	
	<%
		serviceVar++;
		System.out.println("jspService(): service" + serviceVar);
		String content1 = "content1 : "+initVar;
		String content2 = "content2 : "+serviceVar;
		String content3 = "content3 : "+destroyVar;
	%>
	
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>
	
</body>
</html>
